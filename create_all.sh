#!/bin/bash
set -e

bash ./create_base.sh
bash ./create_svg.sh
bash ./create_png.sh