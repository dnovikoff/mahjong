#!/bin/bash
set -e

OUTDIR=html/images

function create {
	EXT=$1
	WIDTH=$2
	DIR=html/image/$WIDTH/
	mkdir -p $DIR
	rm -rf $DIR*

	for i in svg/*.svg
	do
		if test -f "$i" 
		then
			fullname="${i%.*}"
			filename="${fullname##*/}"
			pngname=$DIR$filename.$EXT
			convert -background none -resize $WIDTH $i $pngname
		fi
	done
}

create png 100
create png 50
