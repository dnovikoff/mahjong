# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
[working HTML pages copy](http://dnovikoff.bitbucket.org/mj/)

### How do I get set up? ###

```
git clone https://bitbucket.org/dnovikoff/mahjong.git
cd mahjong
sudo apt-get install npm nodejs-legacy
sudo npm install -g typescript 
sudo npm install -g minify
cd ts
npm install mocha
npm install chai
cd ..
make
```

Typescript files will now be compiled in html/js/mahjong.js
All tests should pass like

```
  ✓ score tests
  ✓ shanten
  ✓ tempai test
  ✓ tempai test waiting
  ✓ not tempai test waiting

  5 passing (38ms)
```

now open html/index.html