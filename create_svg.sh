#!/bin/bash
set -e

RESULT=svg/

RED_COLOR=red
GREEN_COLOR=green
BLACK_COLOR=black

#RED_COLOR=\#AA0000
#GREEN_COLOR=\#507255
#BLACK_COLOR=\#494947

function getPath {
	cat base/$1.svg | tr -d '\r' | tr '\n' '\r' | grep -o "<path[^>]\+>" | tr '\r' '\n'
}

border=$(getPath border)
front=$(getPath front)

function saveSvgContent {
	cat "data/glyph.tpl.svg" | perl -pe "s|\Q{content}|$1|g" | perl -pe "s|green|$GREEN_COLOR|g" | perl -pe "s|red|$RED_COLOR|g" | perl -pe "s|black|$BLACK_COLOR|g" > $RESULT$2
}

function compose {
	p="$border$front"

	for var in "$@"
	do
		if [ "$var" != "dw" ]
		then
			p+="$(getPath $var)"
		fi
	done
	saveSvgContent "$p" $1.svg
}

function create_all {
	prefix=$1
	args="${*:2}"
	for (( i=1; i<10; i++ )); do
		compose $prefix$i $args
	done
}

function compose_all {
	for var in "$@"
	do
		compose $var
	done
}

mkdir -p $RESULT
rm -rf $RESULT*

create_all m man
create_all p
create_all s

compose_all we ws ww wn dw dg dr p5r s5r

# dora

compose m5r man
#back=$(getPath back)
#saveSvgContent "$front$back" back.svg
saveSvgContent '<path fill="orange" transform="scale(0.1,-0.1) translate(0,-800)" d="M92 797h572q33 0 60.5 -32.5t27.5 -56.5v-823q0 -33 -29 -58.5t-50 -25.5h-586q-31 0 -57.5 31t-26.5 66v797q0 40 29 71t60 31z"/>' back.svg


