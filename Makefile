all: build

BUILD=build

typescript:
	make all -C ts

build: typescript
	mkdir -p html/js
	cp ts/build/mahjong.* html/js

image:
	bash create_all.sh
