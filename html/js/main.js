var folder = "image/50/";

var wSize = 50;
var hSize = 67;

function png100() {
	folder = "image/100/";
	wSize = 100;
	hSize = 133;
}

function log(x) {
	console.log(x);
}

function createHint(x) {
	var hint = document.createElement("span");
	hint.innerHTML = x.toUpperCase();
	hint.className = "noborder hint";
	$(hint).hide();
	return hint;
}

function createImage(id) {
	var name = folder + id + ".png";
	var img = document.createElement("img");
	img.className = "tile";
	img.src = name;
	return img;
}

function resize(p, isDouble) {
	p.style.width = hSize + "px";
	p.style.height = wSize + "px";
	p.style.paddingTop = (isDouble?(hSize/2):0) + "px";
}


function createHand(hand, str) {
	$(hand).sortable({
		items : "li:not(.placeholder)",
		tolerance : 'pointer',
		cursor : 'move',
		placeholder : "placeholder",
	}).disableSelection();
	createHandNoMove(hand, str, false);
}


function createHandNoMove(hand, str, skipOnSpace) {
	var type = "";
	var mf = "";
	var doubleLi = null;
	var isDouble = false;
	var needRotate = false;
	for (var i = 0; i < str.length; i++) {
		var cur = str[i];
		if (cur == "=") {
			needRotate = true;
			isDouble = true;
		} else if (cur == "|") {
			var name = folder + "back.png";
			var img = document.createElement("img");
			img.src = name;
			hand.appendChild(img);
		} else if (cur == "$") {
			mf = "r" + mf;
		} else if (cur == "-") {
			needRotate = true;
		} else if (cur == " ") {
			if (skipOnSpace) {
				li = document.createElement("li");
				li.className = "item separator";
				hand.appendChild(li);
			}
			type = "";
		} else if (type == "") {
			type = cur;
		} else {
			var id = type + cur + mf;
			mf = "";

			var li = doubleLi;
			if (!li) {
				li = document.createElement("li");
				li.dragable = "true";
				li.className = "item";
				li.setAttribute("data-value", cur);
				var inner = document.createElement("span");
				inner.className = "tileDiv";

				if (needRotate) {
					inner.className += " tile90Div";
				}
				li.appendChild(inner);
				hand.appendChild(li);
				li = inner;
				li.style.whiteSpace = "nowrap";
			}
			var c = document.createElement("span");
			c.className = "item";
			c.appendChild(createImage(id));
			c.appendChild(createHint(cur));
			li.appendChild(c);

			if (isDouble) {
				doubleLi = li;
				isDouble = false;
			} else {
				doubleLi = null;
			}
			needRotate = false;
		}
	}
}

function rnd(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(str, limit) {
	var len = str.length;
	var str = str.split("");
	for (var i = 0; i < len - 1; i++) {
		var j = rnd(i, len - 1);
		if (j != i) {
			var tmp = str[i];
			str[i] = str[j];
			str[j] = tmp;
		}
	}

	return str.slice(0, limit).join("");
}

function createBaseString(c) {
	var r = "";
	for (var x = 1; x < 10; x++) {
		for (var y = 0; y < c; ++y) {
			r = r + x.toString();
		}
	}
	return r;
}

function createHandByString(str) {
	isCorrect = false;
	var hand = document.getElementById("place4");
	hand.innerHTML = "";
	createHand(hand, "m" + str);
}

function enableHints() {
	$(".hint").toggle();
}
