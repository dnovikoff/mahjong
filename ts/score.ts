/// <reference path="util.ts"/>

module Mahjong {

export class Point {
	constructor(public han: number, public fu: number) {
	}

	mul(m: number): number {
		return Util.round100(this.base() * m);
	}
	
	getHan(): number {
		return this.han;
	}

	private base(): number {
		var han : number = this.han;
		var fu : number = this.fu;
		if (han > 12) {
			return 8000;
		}
		if (han > 10) {
			return 6000;
		}
		if (han > 7) {
			return 4000;
		}
		if (han > 5) {
			return 3000;
		}
		if (han == 5) {
			return 2000;
		}
		var result : number = fu * Math.pow(2, 2 + han);
		if (result > 2000) {
			return 2000;
		}
		if (result < 300) {
			return 300;
		}
		return result;
	}
}

export class PointAnswer {
	constructor(private point: Point, private isDealer: boolean, private isTsumo: boolean) {
	}

	special() : string {
		var han :number = this.point.han;
		var special : string = "";
	
		if (han > 12) {
			special = " якуман";
		} else if (han > 10) {
			special = " санбайман";
		} else if (han > 7) {
			special = " байман";
		} else if (han > 5) {
			special = " ханеман";
		} else if (han == 5 || this.point.mul(4) == 8000) {
			special = " манган";
		}
		return special;
	}

	desc() : string {
		var points : number = this.point.han;
		var result : string = String(points); 
		if (this.point.fu) {
			result += "." + this.point.fu;
		}
		result+= " по "+(this.isTsumo?"цумо":"рон") + (this.isDealer?"+дилер":"");
		return result; 
	}

	toString() : string {
		if (this.isDealer) {
			return String(this.point.mul(this.isTsumo?2:6));
		}
		if (this.isTsumo) {
			return this.point.mul(2) + "/" + this.point.mul(1);
		}
		return String(this.point.mul(4));
	}
}

export class PointTable {
	private table: PointAnswer[] = [];
	static globalTable : PointTable;

	static instance() : PointTable {
		if (!this.globalTable) {
			this.globalTable = new PointTable();
		}
		return this.globalTable;
	}

	constructor() {
		this.fill(1, 30, 110);

		this.pushTsumo(new Point(2, 20));
		this.pushRon(new Point(2, 25));
		this.fill(2, 30, 110);
		this.pushTsumo(new Point(3, 20));
		this.fill(3, 25, 70);
		this.pushTsumo(new Point(4, 20));
		this.fill(4, 25, 40);
	
		this.fillHan(5, 13);
	}

	private pushTsumo(p : Point) : void {
		this.table.push(new PointAnswer(p, true, true));
		this.table.push(new PointAnswer(p, false, true));
	}

	private pushRon(p : Point) : void {
		this.table.push(new PointAnswer(p, true, false));
		this.table.push(new PointAnswer(p, false, false));
	}
	
	private fill(han : number, fuBegin : number, fuEnd : number) : void {
		for (var i = fuBegin; i <= fuEnd; ) {
			var p : Point = new Point(han, i);
			this.pushRon(p);
			this.pushTsumo(p);
			if (i == 25) {
				i = 30;
			} else {
				i += 10;
			}
		}
	}

	private fillHan(hanStart:number, hanEnd:number) : void {
		for (var i = hanStart; i <= hanEnd; ++i) {
			var p : Point = new Point(i, 0);
			this.pushRon(p);
			this.pushTsumo(p);
		}
	}

	getRandomAnswer() : PointAnswer {
		return this.table[Util.rnd(0, this.table.length-1)];
	}
}

} // module Mahjong