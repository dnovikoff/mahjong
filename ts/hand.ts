/// <reference path="wall.ts"/>
/// <reference path="stack.ts"/>

module Mahjong {
	
export class Hand {
	hand : Tiles = [];
	bestStacks : Stack[] = null;
	bestShanten : number = 0;
	bestResult : number = 0;
	setsCount : number = 0;
	pairIndex : number = 0;
	result : number = 0;
	stack : Stack = new Stack();

	clear() : void {
		var hand : Tiles = [];
		for (var i : number = 0; i < TileChars.length; ++i) {
			hand[i] = 0;
		}
		this.hand = hand;
		this.bestStacks = null;
		this.bestShanten = 0;
		this.setsCount = 0;
		this.pairIndex = -1;
		this.result = 0;
		this.stack = new Stack();
		this.bestResult = 0;
	}

	constructor() {
		this.clear();
	}

	sameTilesAs(other : Hand) : boolean {
		for (var i : number = 0; i < TileChars.length; ++i) {
			if ((this.hand[i] > 0) != (other.hand[i] > 0)) {
				return false;
			}
		}
		return true;
	}

	public toString() : string {
		var result : string = "";
		var prevIndex : number = TileIndex.Man;
		var partNumber : number = 0;
		for (var index in TileIndex) {
			if (isNaN(index)) {
				continue;
			}
			index = parseInt(index);
			if (prevIndex == index) {
				continue;
			}
			var part : string = TilePrefixes.charAt(partNumber);
			for (var x : number = prevIndex; x < index; ++x) {
				var count : number = this.hand[x];
				if (count > 0) {
					var cur : string = TileChars.charAt(x);
					part += Util.repeat(cur, count);
				}
			}
			if (part.length > 1) {
				if (result != "") {
					result += " ";
				}
				result += part;
			}
			++partNumber;
			prevIndex = index;
		}
		return result;
	}

	fillFromWall(wall : Wall, count? : number) : void {
		if (!count) {
			count = 13;
		}
		while ((count--) > 0) {
			++this.hand[wall.pop()];
		}
	}

	private shanten7() : number {
		var stack : Stack = new Stack();
		for (var i = 0; i < this.hand.length; ++i) {
			if (this.hand[i] > 1) {
				stack.push([i,i]);
			} else if (this.hand[i] == 1) {
				stack.addMissing([i]);
			}
		}
		var result : number = 6 - stack.length;
		if (result < this.bestShanten) {
			this.bestShanten = result;
			this.bestStacks = [stack];
		}
		return result;
	}

	private shanten13() : number {
		var pairCounted : boolean = false;
		var exist : Tiles = [];
		var missing : Tiles = [];
		var all : Tiles = [];
		for (var i = 0; i < TileChars.length; ++i) {
			var cur = this.hand[i];
			if (Wall.isMiddle(i)) {
				continue;
			}
			all.push(i);
			if (cur == 0) {
				missing.push(i);
				continue;
			}
			if (cur > 1 && !pairCounted) {
				pairCounted = true;
				exist.push(i);
			}
			exist.push(i)
		}

		if (!pairCounted) {
			Util.extend(missing, all);
		}
	
		var result = 13 - exist.length;
		if (result < this.bestShanten) {
			this.bestShanten = result;
			var s : Stack = new Stack();
			s.push(exist, missing);
			this.bestStacks = [s];
		}
		return result;
	}

	private removeChi(x : number) : void {
		// Chi tail is not honor
		if (Wall.isHonor(x+2)) {
			return;
		}
		// Chi tail is same sequence
		if (x%9 > (x+2) %9) {
			return;
		}
	
		var indexes : Tiles = [];
		var missing : Tiles = [];
		for (var s=0; s < 3; ++s) {
			var i = x+s;
			if (this.hand[i] > 0) {
				indexes.push(i);
			} else {
				missing.push(i);
			}
		}
	
		if (indexes.length < 2) {
			return;
		}
	
		this.push(indexes, missing);
		this.shantenNoPair();
		this.pop();
	}

	private shantenNoPair() : void {
		// max sets count in hand is four
		if (this.setsCount >= 4) {
			return;
		}
	
		for (var i = 0; i < TileChars.length; ++i) {
			this.removePon(i);
			this.removeChi(i);
		}
	}

	private removePon(x : number) : void {
		if (this.hand[x] < 2 || this.pairIndex == x) {
			return;
		}
		// remove
		var old = this.hand[x];

		if (this.hand[x] > 2) {
			this.push([x, x, x], []);
		} else {
			this.push([x, x], [x]);
		}

		this.shantenNoPair();
		this.pop();
	}

	push(args : Tiles, missing : Tiles) : void {
		for (var i = 0; i < args.length; ++i) {
			--this.hand[args[i]];
		}
		++this.setsCount;

		this.result += (args.length-1);
		this.stack.push(args, missing);

		if (this.result < this.bestResult) {
			return;
		}

		this.stack.setInHand(this.hand);
		if (this.result == this.bestResult) {
			this.bestStacks.push(this.stack.clone());
		} else {
			this.bestResult = this.result;
			this.bestStacks = [this.stack.clone()];
		}
	}

	pop() : void {
		var args = this.stack.pop();
		for (var i = 0; i < args.length; ++i) {
			++this.hand[args[i]];
		}
		this.result -= (args.length-1);
		--this.setsCount;
	}
	
	static createFromIndexes(indexes : Tiles) : Hand {
		var tmp = new Hand();
		for (var j = 0; j < indexes.length; ++j) {
			tmp.hand[indexes[j]]++;
		}
		return tmp;
	}

	bestStacksToStrings() : string[] {
		if (!this.bestStacks) {
			this.shanten();
		}
		var result : string[] = [];
		var isTempai = (this.bestShanten == 0); 
		
		for (var i = 0; i < this.bestStacks.length; ++i) {
			var stack = this.bestStacks[i].clone();
			if (isTempai && stack.stack.length == 4 && stack.inHand.length == 1) {
				stack.push(stack.inHand, stack.inHand);
			}
			result.push(setsToString(stack.stack.sort(Util.arrLessNum)));
		}
		return Util.sortUnique(result);
	}

	bestOuts() : Tiles {
		if (!this.bestStacks) {
			this.shanten();
		}
		var stack = new Stack();
		for (var i = 0; i < this.bestStacks.length; ++i) {
			stack.mergeMissing(this.bestStacks[i]);
		}
		return stack.normalizeMissing();
	}

	bestOutsString() : string {
		return indexesToString(this.bestOuts());
	}

	shantenNormal() : number {
		this.stack = new Stack();
		this.setsCount = 0;
		this.result = 0;
		this.bestResult = 8 - this.bestShanten;
		this.pairIndex = -1;
	
		// Test for no start pairs in hand
		this.shantenNoPair();
	
		// Pair should not be counted as a set
		this.setsCount = -1;
		// every tile could be a pair
		for (var i = 0; i < TileChars.length; ++i) {
			var cur = this.hand[i];
			// Skip non pairs
			if (cur < 2) {
				continue;
			}
			// Avoid kan problems. See removePon
			this.pairIndex = i;
	
			this.push([i, i],[]);
			this.shantenNoPair();
			this.pop();
		}
		var result = 8 - this.bestResult;
		if (result < this.bestShanten) {
			this.bestShanten = result;
		}
		return result;
	}

	shanten() : number {
		this.bestShanten = 7;
		this.shanten7();
		this.shanten13();
		this.shantenNormal();
		return this.bestShanten;
	}

	static createFromString(x : string) : Hand {
		x += " ";
		var selector = -1;
	
		var hand = new Hand();
		var cnt = 0;
		for (var i=0; i < x.length; ++i) {
			var c = x.charAt(i);
	
			if (c == " ") {
				selector = -1;
				continue;
			}
			
			if (selector == -1) {
				if (c == "m") {
					selector = TileIndex.Man;
				} else if (c  == "p") {
					selector = TileIndex.Pin;
				} else if (c == "s") {
					selector = TileIndex.Sou;
				} else if (c == "w") {
					selector = TileIndex.Wind;
				} else if (c == "d") {
					selector = TileIndex.Dragon;
				} else {
					return null;
				}
				continue;
			}
	
			var num = 0;
	
			if (selector < TileIndex.Wind) {
				if (c >= '1' && c <= '9') {
					num = parseInt(c) -1;
				} else {
					return null;
				}
			} else if (selector == TileIndex.Wind) {
				if (c == "e") {
					num = 0;
				} else if (c == "s") {
					num = 1;
				} else if (c == "w") {
					num = 2;
				} else if (c == "n") {
					num = 3;
				} else {
					return null;
				}
			} else if (selector == TileIndex.Dragon) {
				if (c == "w") {
					num = 0;
				} else if (c == "g") {
					num = 1;
				} else if (c == "r") {
					num = 2;
				} else {
					return null;
				}
			}
			++hand.hand[selector+num];
			++cnt;
			if (cnt == 13) {
				break;
			}
		}
		return hand;
	}
};

function indexesToString(indexes : Tiles) : string {
	var hand = Hand.createFromIndexes(indexes);
	return hand.toString();
}
	
function setsToString(stack : SetArray) : string {
	var result = "";
	for (var i = 0; i < stack.length; ++i) {
		if (result != "") {
			result += " ";
		}
		result += indexesToString(stack[i]);
	}
	return result;
}

} // module Mahjong