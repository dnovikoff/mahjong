/// <reference path="util.ts"/>
/// <reference path="types.ts"/>

module Mahjong {

export class Wall {
	private wall: number[];

	constructor(start?: number, end?: number) {
		if (!end) {
			start = TileIndex.Man;
			end = TileIndex.End;
		}
	
		var result : number[] = [];
		for (var x = 0; x < 4; ++x) {
			for (var i = start; i < end; ++i) {
				result.push(i);
			}
		}
		this.wall = result;
		this.shuffle();
	}

	private swap(i: number, j: number) : void {
		if (j == i) {
			return;
		}
		var tmp : number = this.wall[i];
		this.wall[i] = this.wall[j];
		this.wall[j] = tmp;
	}

	private shuffle() : void {
		var len : number = this.wall.length;
		for (var i = 0; i < len - 1; ++i) {
			this.swap(i, Util.rnd(i, len - 1));
		}
	}

	public pop() : number {
		return this.wall.pop();
	}

	static isHonor(x : number) : boolean {
		return x >= 9*3;
	}

	static isTerm(x : number) : boolean {
		var num : number = (x%9)+1;
		return !this.isHonor(x) && (num == 1 || num == 9);
	}

	static isTermOrHonor(x : number) : boolean {
		return this.isHonor(x) || this.isTerm(x);
	}

	static isMiddle(x : number) : boolean {
		return !this.isTerm(x) && !this.isHonor(x);
	}
};

} // module Mahjong