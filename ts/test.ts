/// <reference path="typings/mocha/mocha.d.ts" />
/// <reference path="typings/chai/chai.d.ts" />

import chai = require('chai');
import mj = require('mahjong');

var expect = chai.expect;

function score(han, fu, m) {
	return (new mj.Point(han, fu)).mul(m);
}

describe('Different tests', function() {
	it('should be 6', function(done) {
		expect(score(1,40,4)).to.equals(1300);
		expect(score(1,40,6)).to.equals(2000);
	
		expect(score(2,30,4)).to.equals(2000);
		expect(score(2,30,6)).to.equals(2900);
	
		expect(score(2,30,1)).to.equals(500);
		expect(score(2,30,2)).to.equals(1000);
	
		expect(score(1,40,1)).to.equals(400);
		expect(score(1,40,2)).to.equals(700);
	
		expect(score(3, 70, 1)).to.equals(2000);
		expect(score(3, 70, 2)).to.equals(4000);
		expect(score(3, 70, 4)).to.equals(8000);
		expect(score(3, 70, 6)).to.equals(12000);
	
		expect(score(4, 40, 1)).to.equals(2000);
		expect(score(4, 40, 2)).to.equals(4000);
		expect(score(4, 40, 4)).to.equals(8000);
		expect(score(4, 40, 6)).to.equals(12000);
	
		expect(score(5,15.5, 1)).to.equals(2000);
		expect(score(5,15.5, 2)).to.equals(4000);
		expect(score(5,15.5, 4)).to.equals(8000);
		expect(score(5,15.5, 6)).to.equals(12000);
	
		expect(score(13,15.5, 1)).to.equals(8000);
		expect(score(13,15.5, 2)).to.equals(16000);
	
		expect(score(13,15.5, 4)).to.equals(32000);
		expect(score(13,15.5, 6)).to.equals(48000);
		done();
	});
});
