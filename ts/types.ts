module Mahjong {

export enum TileIndex {
	Man = 0,
	Pin = Man+9,
	Sou = Pin+9,
	Wind = Sou+9,
	Dragon = Wind+4,
	End = Dragon+3,
};
	
export const TilePrefixes : string = "mpswd";
export const TileChars : string = "123456789123456789123456789eswnwgr";

export type Tiles = number[];
export type SetArray = Tiles[];

} // module Mahjong