var chai = require('chai');
var expect = chai.expect;
var Mahjong = require('../build/test.js');

function getHand(x) {
	return Mahjong.Hand.createFromString(x);
}

function expectWait(x) {
	var hand = getHand(x);
	if (hand == null) {
		return expect("not parsed");
	} else if (hand.shanten() != 0) {
		return expect("not tempai hand " + hand.shanten());
	}
	return expect(hand.bestOutsString());
}

it("tempai test", function () {
	expectWait("p3334567788999").to.equals("p36789"); // "multiple"
	expectWait("weeessswwwnnn dr").to.equals("dr"); // "one only"
});

it("tempai test waiting", function () {
	var hand = getHand("weeessswwwnnn dr");
	expect(hand.shanten()).to.equals(0);
	var r = hand.bestStacksToStrings();
	expect(r.length).to.equals(1);
	r = r.pop();
	expect(r).to.equals("weee wsss wwww wnnn dr");
});

it("not tempai test waiting", function () {
	var hand = getHand("weeessswwwnn drg");
	expect(hand.shanten()).to.equals(1);
	var r = hand.bestStacksToStrings();
	expect(r.length).to.equals(1);
	r = r.pop();
	expect(r).to.equals("weee wsss wwww wnn");
});