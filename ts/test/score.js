var chai = require('chai');
var expect = chai.expect;
var Mahjong = require('../build/test.js');

function expectScore(han, fu, m) {
    return expect(new Mahjong.Point(han, fu).mul(m));
}

it('score tests', function () {
    expectScore(1, 40, 4).to.equals(1300);
    expectScore(1, 40, 6).to.equals(2000);
    expectScore(2, 30, 4).to.equals(2000);
    expectScore(2, 30, 6).to.equals(2900);
    expectScore(2, 30, 1).to.equals(500);
    expectScore(2, 30, 2).to.equals(1000);
    expectScore(1, 40, 1).to.equals(400);
    expectScore(1, 40, 2).to.equals(700);
    expectScore(3, 70, 1).to.equals(2000);
    expectScore(3, 70, 2).to.equals(4000);
    expectScore(3, 70, 4).to.equals(8000);
    expectScore(3, 70, 6).to.equals(12000);
    expectScore(4, 40, 1).to.equals(2000);
    expectScore(4, 40, 2).to.equals(4000);
    expectScore(4, 40, 4).to.equals(8000);
    expectScore(4, 40, 6).to.equals(12000);
    expectScore(5, 15.5, 1).to.equals(2000);
    expectScore(5, 15.5, 2).to.equals(4000);
    expectScore(5, 15.5, 4).to.equals(8000);
    expectScore(5, 15.5, 6).to.equals(12000);
    expectScore(13, 15.5, 1).to.equals(8000);
    expectScore(13, 15.5, 2).to.equals(16000);
    expectScore(13, 15.5, 4).to.equals(32000);
    expectScore(13, 15.5, 6).to.equals(48000);
});
