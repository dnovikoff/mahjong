var chai = require('chai');
var expect = chai.expect;
var Mahjong = require('../build/test.js');

function expectShanten(x) {
	var hand = Mahjong.Hand.createFromString(x);
	if (!x) {
		return expect("not shanten");
	}
	return expect(hand.shanten());
}

it('shanten', function () {
	expectShanten("p1231231231234").to.equals(0); // "Tempai" 
	expectShanten("p12 s567 m19 www").to.equals(4); // "Regular" 
	expectShanten("p112233").to.equals(3); // "Seven pairs" 
	expectShanten("p112233 wwwsseen").to.equals(0); // "Seven pairs tempai" 

	expectShanten("p112233 wwwwween").to.equals(1); // "Seven different pairs" 

	expectShanten("p19 s19 m19 dwgr wws").to.equals(2); // "Kokushi" 

	expectShanten("p199 s19 m19 dwgr wws").to.equals(1); // "Kokushi one pair" 
	expectShanten("p199 s199 m19 dwgr wws").to.equals(1); // "Kokushi two pairs" 

	expectShanten("p1379 s1379 wwsen dr").to.equals(4); // "Regular 4 sets" 
	expectShanten("p1379 s1379 m1379 ww").to.equals(4); // "Regular 6 sets" 
});
