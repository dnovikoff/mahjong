/// <reference path="types.ts"/>

module Mahjong {

export class Stack {
	length : number = 0;
	stack : SetArray = [];
	inHand : Tiles = [];
	missing : SetArray = [];

	clone() : Stack {
		var copy = new Stack();
		copy.length = this.length;
		copy.stack = Util.clone(this.stack);
		copy.inHand = Util.clone(this.inHand);
		copy.missing = Util.clone(this.missing);
		return copy;
	}
	
	normalizeMissing() : Tiles {
		var result : Tiles = [];
		for (var i = 0; i < this.missing.length ; ++i) {
			Util.extend(result, this.missing[i]);
		}
		Util.extend(result, this.inHand);
		result = Util.sortUnique(result);
		return result;
	}

	addMissing(m?: Tiles) : void {
		if (!m) {
			m = [];
		}
		this.missing.push(m); 
	}

	setInHand(h : Tiles) : void {
		var result : Tiles = [];
		for (var i = 0; i < TileChars.length; ++i) {
			for (var j : number = 0; j < h[i]; ++j) {
				result.push(i);
			}
		}
		this.inHand = result;
	}

	mergeMissing(x : Stack) : void {
		Util.extend(this.inHand, x.inHand);
		Util.extend(this.missing, x.missing);
	}

	push(x : Tiles, missing? : Tiles) : void {
		this.addMissing(missing);
		this.stack.push(x);
		this.length = this.stack.length;
	}
	
	pop() : Tiles {
		this.missing.pop();
		var r : Tiles =  this.stack.pop();
		this.length = this.stack.length;
		return r;
	}
};

} // module Mahjong