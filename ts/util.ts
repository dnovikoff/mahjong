interface Error {
	stack: string; 
};

module Util {
	
export function round100(x: number): number {
	return Math.ceil(x/100) * 100;
}
	
export function rnd(min : number, max : number) : number {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function repeat(str : string, count : number) : string {
	var r : string = "";
	for (var x=0; x < count; ++x) {
		r += str;
	}
	return r;
}

export function extend<T>(arr : T[], other : T[]) : T[] {
	for (var i: number = 0; i < other.length; ++i) {
		arr.push(other[i]);
	}
	return arr;
}

export function stacktrace() : string { 
	var err = new Error();
	return err.stack;
}

export function clone<T>(item : T) : T {
	return JSON.parse(JSON.stringify(item));
}
	
export function sortUnique<T>(arr : T[]) : T[] {
	return arr.sort(function(a : T,b : T) : number {
		return (a > b) ? 1 : -1;
	}).filter(function(el : T, i : number, a : T[]) {
		return (i==a.indexOf(el));
	});
}

export function arrLess<T>(lhs : T[], rhs : T[]) : boolean {
	var len : number = Math.min(lhs.length, rhs.length);
	for (var i=0; i < len; ++i) {
		if (lhs[i] < rhs[i]) {
			return false;
		} else if (lhs[i] > rhs[i]) {
			return true;
		}
	}
	return lhs.length < rhs.length;
}

export function arrLessNum<T>(lhs : T[], rhs : T[]) : number {
	return arrLess(lhs, rhs) ? 1 : -1;
}

} // module Mahjong